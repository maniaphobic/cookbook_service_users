# Default attribute assignments

# We assign an empty hash to the "service_users" attribute. Cookbook
# callers should assign a meaningful value in an environment file.

node.default['service_users'] = {}
