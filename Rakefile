configuration = {
  default_task: :test,
  foodcritic: {
    epic_fail: true,
    exclude: %w(test),
    tags: %w(~FC064 ~FC065)
  }
}

task default: configuration.fetch(:default_task, :test)

task test: [:cookbook_unit, :cookbook_style, :ruby_style]

desc 'Invoke cookbook unit tests'
task :cookbook_unit do
  sh %(chef exec rspec)
end

desc 'Invoke cookbook style tests'
task :cookbook_style do
  options = configuration.fetch(:foodcritic, {})
  sh [
    'chef exec foodcritic',
    options.fetch(:epic_fail, false) ? '--epic-fail any' : '',
    options.fetch(:exclude, []).map { |path| "--exclude #{path}" }.join(' '),
    options.fetch(:tags, []).map { |tag| "--tags #{tag}" }.join(' '),
    '.'
  ].join(' ')
end

desc 'Invoke Ruby style tests'
task :ruby_style do
  sh %(chef exec cookstyle)
end
