require 'spec_helper'

describe 'service_users::default' do
  context group('genus') do
    it { should exist }
  end

  context group('kingdom') do
    it { should exist }
  end

  context group('alpha') do
    it { should exist }
  end

  context user('alpha') do
    it { should exist }
    it { should belong_to_primary_group 'alpha' }
    %w(genus kingdom phylum).map do |group_name|
      it { should belong_to_group group_name }
    end
    it { should have_authorized_key 'ssh-rsa ...' }
  end

  context group('phylum') do
    it { should exist }
  end

  context group('beta') do
    it { should_not exist }
  end

  describe user('beta') do
    it { should_not exist }
  end
end
