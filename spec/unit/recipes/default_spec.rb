require 'spec_helper'

describe 'service_users::default' do
  context 'when the service_users attribute is populated' do
    let(:chef_run) do
      ChefSpec::SoloRunner.new(
        platform: 'centos',
        version: '7.2.1511'
      ) do |node|
        node.override['service_users'] = {
          'groups' => {
            'create_me' => {
              'action' => 'create'
            },
            'remove_me' => {
              'action' => 'remove'
            }
          }
        }
      end.converge(described_recipe)
    end

    it 'should create a group designated for creation' do
      expect(chef_run).to create_users_manage('create_me')
    end

    it 'should remove a group designated for removal' do
      expect(chef_run).to remove_users_manage('remove_me')
    end
  end

  context 'when the service_users attribute is nil' do
    let(:chef_run) do
      ChefSpec::SoloRunner.new(
        platform: 'centos',
        version: '7.2.1511'
      ).converge(described_recipe)
    end

    it 'should not raise an exception' do
      expect { chef_run }.to_not raise_error
    end
  end
end
