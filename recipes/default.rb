#
# Cookbook Name:: service_users
# Recipe:: default
#
# Copyright (c) 2017 The Authors, All Rights Reserved.

# Iterate over the group definitions
node
  .fetch('service_users', {})
  .fetch('groups', {}).map do |group_name, group_attrs|
  group_attrs = {} unless group_attrs.respond_to?(:fetch)
  users_manage group_name do
    action   group_attrs.fetch('action', 'create').to_sym
    data_bag group_attrs.fetch('data_bag', 'users')
  end
end
