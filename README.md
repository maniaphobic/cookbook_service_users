# Service Users Cookbook

I abide by the Chef principle that a cookbook designed to manage a
service should control all aspects of that service, and further
believe that no _other_ automation system should attempt to manage any
aspect of the service, lest instability result. I call this
"configuration sovereignty".

Applying this principle to user accounts, if an application requires a
user account, then the application's cookbook should manage that
account, not a generic "users" cookbook.

Unfortunately, this model offers no guidance for a service that needs
to access a target system via SSH, but that otherwise installs no
software or configuration on that system. I developed the Service
Users cookbook to address this use case.

# Cookbook Dependencies

This cookbook depends on the
community's
[users cookbook](https://supermarket.chef.io/cookbooks/users/) and
leverages its `users_manage` resource. `users_manage` loads account
information from a data bag named `users`, which must be populated
with an item file for each user prior to Service Users executing.

# Usage

* Populate the `service_users` node attribute as described in
  _Attributes_ below.
* Create a `users` data bag and an item for each user. See the
  `users` cookbook's documentation for the format.
* Add `recipe[service_users::default]` to the run list
* If the user needs elevated privileges and you're using the
  community's
  [sudo cookbook](https://supermarket.chef.io/cookbooks/sudo/), add
  its group to `authorization/sudo/groups` or its username to
  `authorization/sudo/users`.
* Likewise, if you're restricting the groups that can authenticate to
  your hosts via
  the [sshd cookbook](https://supermarket.chef.io/cookbooks/sshd/),
  add each group to `sshd/sshd_config` as `AllowGroups <group>`.

# Attributes

This cookbook defines a single default attribute:

    default['service_users'] = {}

Callers should override this value at a higher precedence level,
typically in an environment.

The `service_users` attribute should be a hash conforming to this
general structure (JSON notation):

    service_users: {
      "groups": {
        "<group name>": {
          "action": "<action>"
        },
        ...
      }
    }

`groups` is itself a hash containing any number of entries, with each
key being a Unix group name to manage. Each group's `action` entry is
optional and defaults to `create`. Its value is passed to the
`users_manage` resource, which also accepts `remove` to remove a
group. To accept the default, declare the group with a `null` value:

    service_users: {
      "groups": {
        "<group name>": null
      }
    }

# Populating the "users" Data Bag

In addition to defining the managed groups in `service_users/groups`,
one must create a `users` data bag and populate it with an item for
each user. Every user must belong to one of the defined groups. The
`users` cookbook's documentation describes the item file's format, but
I want to emphasize the requirement that each user's primary and
secondary groups _must_ appear in the item's `groups` list.

By way of example, assume a `service_users` attribute defined as
follows:

    service_users: {
      "groups": {
        "admins": null,
        "developers": null
      }
    }

This declares that Chef should manage two groups, `admins` and
`developers`. If user `ann` should belong to `admins` and user
`debbie` should belong to `developers`, then their `users` items would
be:

    {
      "id": "ann",
      "groups": ["admins"]
    }

    {
      "id": "debbie",
      "groups": ["developers"]
    }

The `groups` specification in an item file associates the user with a
group defined in the `service_users` attribute.
